import {TaskProvider} from './provider';
import {formatters, parseDate, sortTasks, calcColorStyles, isoDate} from './format';
import {EventEmitter} from '../tool/events';
import {init as styleInit} from '../styles/style';
import {init as stylesInit} from '../styles/main';
class StreamEater {
    eat(line) {
        // Implement me
    }

    end() {
    }
}

const specialReports = [
    "burndown.daily",
    "burndown.monthly",
    "burndown.weekly",
    "calendar",
    "colors",
    "export",
    "ghistory.annual",
    "ghistory.monthly",
    "history.annual",
    "history.monthly",
    "information",
    "summary",
    "timesheet",
    "projects",
    "tags",
];

const dayNames = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

class ToArrayEater extends StreamEater {

    constructor() {
        super();
        this.data = [];
    }

    eat(line) {
        this.data.push(line);
    }
}

class ToStringEater extends ToArrayEater {
    str() {
        return this.data.join('\n').trim();
    }
}

export class TaskController {

    constructor() {
        this.fixParams = ['rc.confirmation=off', 'rc.color=off', 'rc.verbose=nothing'];
        this.events = new EventEmitter();
        this.timers = {};
    }

    async call(args, out, err, options) {
        try {
            const result = await this.provider.call(this.fixParams.concat(args), out, err, options);
            if (out && out.end) out.end(result);
            if (err && err.end) err.end(result);
            return result;
        } catch (e) {
            console.log('Error calling task:', e);
            return -1;
        };
    }

    err(message) {
        this.events.emit('notify:error', message);
    }

    info(message) {
        this.events.emit('notify:info', message);
    }

    streamNotify(evt='notify:error') {
        const stream = new ToStringEater();
        stream.end = () => {
            const message = stream.str();
            if (message) { // Not empty
                this.events.emit(evt, message);
            };
        };
        return stream;
    }

    notifyChange() {
        this.events.emit('change');
    }

    async callStr(args) {
        const out = new ToStringEater();
        const code = await this.call(args, out, this.streamNotify());
        if (code != 0) { // Invalid
            return undefined;
        };
        return out.str();
    }

    async init(config={}) {
        delete this.provider;
        config.onQuestion = (text, choices) => {
            return new Promise((resp, rej) => {
                this.events.emit('question', text, choices, resp, rej);
            });
        };
        config.onTimer = async (type) => {
            if (type == 'sync') {
                const success = await this.sync();
                if (success) { // Show info
                    this.info('Automatically synchronized')
                };
            };
        };
        this.foreground = true;
        config.onState = async (state, mode) => {
            // console.log('State change:', state);
            if (state == 'active') { // Refresh everything
                this.foreground = true;
            };
            if (state == 'background') { // Focus out
                this.foreground = false;
            };
            if (state == 'sync' && mode) { // Sync finish
                this.notifyChange();
            };
            if (state == 'online') { // Sync
                const {auto} = this.timers.extra;
                // console.log('Online config:', online, mode);
                if (auto) {
                    // Only when enabled
                    const success = await this.sync();
                    if (success) { // Show info
                        this.info('Automatically synchronized')
                    };
                };
            };
        };
        const provider = new TaskProvider(config);
        if (!await provider.init()) { // OK
            return false;
        };
        this.provider = provider;
        this.configCache = await this.loadConfig();
        this.setupSync();
        this.scheduleSync();
        this.dueDays = parseInt(this.config('due')) || 7;
        this.defaultCmd = this.config('default.command') || 'next';
        this.panesConfig = await this.loadPanesConfig();
        await this.loadUDAs();
        this.multiline = {};
        (this.config('ui.multiline') || '').split(',').map((item) => item.trim()).forEach((item) => {
            if (item) this.multiline[item] = true;
        });
        this.multilineSep = this.config('ui.multiline.separator') || '\\n';
        this.reportExtra = this.config('ui.report.extra.', true);
        const css = this.config('ui.style.', true);
        styleInit(css);
        stylesInit(css, this);
        this.cssConfig = css;
        this.calendarConfig = await this.loadCalendar();
        const reportsConf = this.config('ui.reports');
        if (reportsConf) {
            this.reportsSublist = reportsConf.split(',').map(x => x.trim());
        };
        if (provider.start) await provider.start(this);
        return true;
    }

    readLimit(value) {
        if (value === 'off') { // Reset limit
            return -1;
        };
        if (value && parseInt(value) > 0)
            return parseInt(value);
        return 0;
    }

    providerInfo() {
        return this.provider.info || {};
    }

    fromCalendar() {
        let dt = new Date();
        dt.setDate(1);
        return dt;
    }

    async loadPanesConfig() {
        const config = this.config('ui.pane.', true);
        const pos = (config['popup.position'] || '').split('x');
        let conf = {
            expanded: config['tasks.expanded'] == 'off'? false: true,
            limit: this.readLimit(config['tasks.limit']),
            left: config.left,
            right: config.right,
            pins: [],
            pages: [],
            tags: config.tags || 'scroll',
            projects: config.projects || 'scroll',
            reports: config.reports || 'scroll',
            contexts: config.contexts || 'compact',
            popup: {
                enabled: config.popup == 'on',
                report: config['popup.report'] || 'active',
                filter: config['popup.filter'],
                x: pos[0],
                y: pos[1],
                width: pos[2],
                height: pos[3],
                corner: config['popup.corner'],
                screen: config['popup.screen'],
            },
            uda: config.uda? config.uda === 'none'? false: config.uda.split(','): true,
        };
        const pageParser = (item) => {
            let report = item.trim();
            let filter = '';
            const sp = report.indexOf(' ');
            if (sp != -1) { // Split
                filter = report.substr(sp).trim();
                report = report.substr(0, sp).trim();
            };
            return {report, filter};
        };
        if (config.pin) { // Have pinned panes
            conf.pins = config.pin.split(',').map(pageParser);
        };
        conf.pages = (config['default'] || this.defaultCmd).split(',').map(pageParser);
        return this.provider.configurePanes(conf);
    }

    async loadCalendar() {
        const dayNo = (name, def=-1) => {
            const index = dayNames.indexOf(name);
            if (index == -1) return def;
            return index;
        };
        const conf = this.config('ui.calendar.', true);
        let result = {
            start: dayNo(conf['weekstart'], 0),
            pane: conf['pane'] || 'right',
            command: conf['cmd'] || 'due',
            commandAlt: conf['cmd.alt'] || 'wait',
            filter: conf['filter'] || 'due.after',
            filterAlt: conf['filter.alt'] || 'due.before',
            weekends: [0, 6],
        };
        if (conf['weekends']) {
            result.weekends = [];
            conf['weekends'].split(',').forEach((item) => {
                const num = dayNo(item.trim());
                if (num != -1) result.weekends.push(num);
            });
        }
        return result;
    }

    async loadUDAs() {
        const conf = this.config('uda.', true);
        this.udas = {};
        for (let key in conf) {
            const parts = key.split('.');
            if (parts.length != 2) continue;
            let obj = this.udas[parts[0]] || {};
            obj[parts[1]] = conf[key];
            this.udas[parts[0]] = obj;
        }
    }

    udaPanels() {
        const {uda} = this.panesConfig;
        if (uda === false) {
            return [];
        };
        let result = [];
        for (let key in this.udas) {
            const value = this.udas[key];
            if (value.type !== 'string') { // Invalid type
                continue;
            };
            if (!value.values) { // No pre-defined values
                continue;
            };
            if (uda === true || uda.includes(key)) { // In list or all
                result.push({
                    id: key,
                    label: value.label || key,
                    values: value.values.split(','),
                });
            };
        };
        if (uda === true) { // Sort by id
            result.sort((a, b) => {
                return a.id > b.id? 1: -1;
            });
        } else { // Sort defined by user
            result.sort((a, b) => {
                return uda.indexOf(a.id) - uda.indexOf(b.id);
            });
        };
        return result;
    }

    async specialList(type) {
        let report = this.config(`ui.report.${type}`, true);
        if (!report) report = {};
        return report[''] || this.defaultCmd;
    }

    setupSync() {
        const timers = this.config('ui.sync.', true);
        this.timers = this.provider.configureSync(timers);
        ['normal', 'error', 'commit'].forEach((field) => {
            if (timers[field] !== undefined) { // Overrid
                this.timers[field] = parseInt(timers[field], 10)
            };
        });
        this.timers.extra = this.config('ui.sync.extra.', true) || {};
        console.log('setupSync:', this.timers);
    }

    confBool(value) {
        if (value === undefined || value === '') { // Empty
            return undefined;
        };
        if (['on', 'y', 'yes', 'true', 't', '1'].includes(value)) { // True
            return true;
        };
        return false;
    }

    scheduleSync(type='normal') {
        const timeout = this.timers[type] || this.timers.normal || 0;
        if (timeout > 0 && this.provider) { // Have timeout
            this.provider.schedule(timeout*60, 'sync', true, this.timers);
        };
    }

    async reportInfo(report) {
        let result = {
            cols: [],
            report: report,
            filter: '',
            precedence: [],
        };
        let desc = [];
        const ruleConf = this.config('rule.precedence.color');
        if (ruleConf) { // Have
            result.precedence = ruleConf.split(',').reverse();
        };
        const config = this.config(`report.${report}.`, true);
        for (let key in config) {
            if (key == 'columns') {
                let columnsStr = config[key];
                if (this.reportExtra.columns) columnsStr += `,${this.reportExtra.columns}`;
                for (let s of columnsStr.split(',')) {
                    let cm = s.indexOf('.');
                    if (cm == -1) {
                        result.cols.push({
                            field: s,
                            full: s,
                            display: '',
                        });
                    } else {
                        result.cols.push({
                            field: s.substr(0, cm),
                            display: s.substr(cm+1),
                            full: s,
                        });
                    }
                }
            }
            if (key == 'labels') {
                let labelsStr = config[key];
                if (this.reportExtra.labels) labelsStr += `,${this.reportExtra.labels}`;
                desc = labelsStr.split(',');
            }
            if (key == 'description') {
                result.description = config[key];
            }
            if (key == 'filter') {
                result.filter = config[key];
            }
        }
        if (!result.filter && !result.cols.length) { // No such report
            return undefined;
        };
        if (!result.description) result.description = result.filter;
        if (desc.length == result.cols.length) {
            // Same size -> add label
            for (var i = 0; i < desc.length; i++) {
                result.cols[i].label = desc[i];
            }
        } else { // Failsafe
            for (col of result.cols) {
                col.label = col.field;
            }
        }
        return result;
    }

    async exp(cmds) {
        let cmd = ['rc.json.array=off'].concat(cmds);
        cmd.push('export');
        let result = [];
        const code = await this.call(cmd, {
            eat(line) {
                if (!line) {
                    return;
                }
                // Parse and save
                try {
                    let json = JSON.parse(line);
                    if (json.depends && !Array.isArray(json.depends)) { // Old style
                        json.depends = json.depends.split(',');
                    };
                    json.unique = json.id || json.uuid;
                    result.push(json);
                } catch (e) {
                    console.log('JSON error:', line);
                }
            }
        }, this.streamNotify());
        if (code != 0) {
            console.log('Failure:', cmd, code);
            return undefined;
        }
        return result;
    }

    async count(report, filter) {
        const info = await this.reportInfo(report);
        if (!info) {
            this.err('Report does not exist');
            return undefined;
        }
        let taskUuids = await this.reportUuids(report, filter);
        if (!taskUuids) { // Failed
            this.err('Report load failure');
            return undefined;
        };
        return taskUuids.length;
    }

    async reportUuids(report, filter) {
        let result = [];
        let cmd = [report];
        if (filter) { // Add extra filter
            cmd.push(filter);
        };
        cmd.push(`rc.report.${report}.columns=uuid`);
        cmd.push(`rc.report.${report}.labels=uuid`);
        const code = await this.call(cmd, {
            eat(line) {
                if (!line) {
                    return;
                }
                result.push(line);
            }
        }, this.streamNotify());
        if (code > 1) {
            console.log('Failure:', cmd, code);
            return undefined;
        }
        return result;
    }

    async filter(report, filter, info, sortMode='list') {
        if (!info || info.report != report) {
            info = await this.reportInfo(report);
        }
        if (!info) {
            this.err('Report does not exist');
            return undefined;
        }
        info.tasks = []; // Reset
        let cmd = [];
        let taskUuids = await this.reportUuids(report, filter);
        if (!taskUuids) { // Failed
            this.err('Report load failure');
            return undefined;
        };
        if (!taskUuids.length) { // No items
            return info;
        };
        if (this.panesConfig.limit > 0) { // Limit number of tasks in output
            taskUuids = taskUuids.slice(0, this.panesConfig.limit);
        };
        const expResult = await this.exp([taskUuids.map(uuid => `uuid:${uuid.substr(0,8)}`).join(' or ')]);
        if (!expResult) {
            this.err('Export failure');
            return undefined;
        };
        info.tasks = expResult;
        let hasDepends = false;
        info.cols.forEach((item) => {
            if (item.field == 'depends') { // Need a list
                hasDepends = true;
            };
        });
        if (hasDepends) { // Call uuids and load depends
            let all_uuids = {};
            for (var i = 0; i < info.tasks.length; i++) {
                let task = info.tasks[i];
                if (task.depends) { // Make export call
                    task.depends.forEach((uuid) => all_uuids[uuid] = null);
                };
            };
            const uuids = Object.keys(all_uuids)
                .map((uuid) => `uuid:${uuid.substr(0, 8)}`).join(' or ');
            if (uuids) { // Have keys => load all
                const uuidsTasks = await this.exp([uuids]);
                if (uuidsTasks) { // OK
                    uuidsTasks.forEach((t) => all_uuids[t.uuid] = t);
                    for (var i = 0; i < info.tasks.length; i++) {
                        let task = info.tasks[i];
                        if (task.depends && task.depends.length) { // Make export call
                            task.dependsTasks = task.depends
                                .map((uuid) => all_uuids[uuid])
                                .filter((t) => t && t.id>0)
                        };
                    };
                };
            };
        };
        // Calculate sizes
        info.cols.forEach((item) => {
            item.visible = false;
            item.multiline = false;
            if ('status' == item.field) {
                return;
            }
            if (this.multiline[item.field]) { // Multi - skip
                item.multiline = true;
                item.visible = true;
            };
            if (item.field == 'depends') { // Need a list
                hasDepends = true;
            };
            let handler = formatters[item.field];
            if (!handler) { // Not supported
                if (this.udas[item.field]) {
                    handler = formatters.uda;
                }
            };
            // Colled max size
            let max = 0;
            // console.log('Col:', item.field);
            info.tasks.forEach((task) => {
                const val = handler? handler(task, item.display, item, this): (task[item.field] || '');
                task[`${item.full}_`] = val;
                if (item.multiline) {
                    let lines = [''];
                    if (val) { // Split
                        lines = val.split(this.multilineSep);
                    };
                    task[`${item.full}_lines`] = lines;
                    return;
                };
                if (val.length > max) {
                    max = val.length;
                };
                // console.log('Format:', item.field, val, item.display);
            });
            if (max > 0 || ['id', 'uuid', 'description'].indexOf(item.field) != -1) { // Visible
                item.visible = true;
                item.width = Math.max(max, item.label.length);
                // console.log('Will display:', item.label, item.width);
            };
        });
        info.tasks = sortTasks(info, taskUuids, sortMode);
        info.tasks.forEach((task) => {
            task.styles = calcColorStyles(task, info.precedence, this);
        });
        return info;
    }

    async undo() {
        const code = await this.call(['undo'], null, this.streamNotify(), {
            slow: true,
            question: true,
        });
        // console.log('Undo:', code);
        if (code == 0) { // Success
            this.notifyChange();
            this.scheduleSync('commit');
        };
        return code;
    }

    async cmd(cmd, input, tasks=[], silent=false) {
        let cmds = [];
        const ids = tasks.map((task) => {
            return task.id || task.uuid_ || task.uuid;
        });
        if (ids.length) {
            cmds.push(ids.join(','));
        };
        cmds.push(cmd);
        cmds.push(input);
        // console.log('cmd', cmds);
        const code = await this.call(cmds, this.streamNotify('notify:info'), this.streamNotify(), {
            slow: true,
            question: true,
        });
        // console.log('cmd result', cmds, code);
        if (code === 0) {
            if (!silent) {
                this.notifyChange();
                this.scheduleSync('commit');
            };
            return true;
        };
        return false;
    }

    async cmdRaw(cmd, handler) {
        const out = new ToArrayEater();
        const err = new ToArrayEater();
        const stream2result = (stream, type) => {
            return stream.data.map((line, index) => {
                return {
                    line, type,
                    key: `${type}_${index}`
                };
            });
        };
        let code;
        try {
            code = await this.provider.call([cmd], out, err, {
                slow: true,
                question: true,
                flush: () => {
                    handler && handler({lines: stream2result(out, 'out')});
                },
            });
        } catch (e) {
            console.log('Error:', e);
            code = -1;
        };
        let result = stream2result(out, 'out').concat(stream2result(err, 'error'));
        result.push({
            type: 'info',
            line: `Exit code: ${code}`,
            key: 'exit'
        });
        return {
            lines: result,
        };
    }

    async sync() {
        this.events.emit('sync:start');
        const code = await this.call(['sync'], null, this.streamNotify(), {
            slow: true,
            question: true,
        });
        this.events.emit('sync:finish');
        if (code == 0) {
            this.notifyChange();
            this.scheduleSync();
        } else {
            this.scheduleSync('error');
        }
        return code == 0;
    }

    async loadConfig() {
        const reg = /^([a-z0-9_\.]+)=(.+)$/;
        let result = {}; // Hash
        let args = ['_show'];
        await this.call(args, {
            eat(line) {
                if (line) {
                    // Our case
                    const m = line.match(reg);
                    if (m) {
                        let key = m[1].trim();
                        result[key] = m[2].trim();
                    }
                }
            }
        }, this.streamNotify());
        return result;
    }

    config(prefix, strip_prefix) {
        let result = {}; // Hash
        if (!strip_prefix) { // Simple return
            return this.configCache[prefix];
        };
        for (let key in this.configCache) {
            if (key.indexOf(prefix) == 0) { // Starts with
                result[key.substr(prefix.length)] = this.configCache[key];
            };
        }
        return result;
    }

    async tags(expanded) {
        const reg = /^(.+)\s(\d+)$/;
        let result = []; // List
        let added = {};
        if (expanded) { // All unique tags
            await this.call(['_unique', 'tag'], {
                eat(line) {
                    if (line) {
                        const tag = line.split(',')[0];
                        if (added[tag]) return;
                        added[tag] = true;
                        result.push({
                            name: tag,
                        });
                    }
                }
            }, this.streamNotify());
            return result;
        } else {
            await this.call(['tags'], {
                eat(line) {
                    const m = line.match(reg);
                    if (m) {
                        const tag = m[1].trim();
                        result.push({
                            name: tag,
                            count: parseInt(m[2], 10),
                        });
                        added[tag] = true;
                    }
                }
            }, this.streamNotify());
        }
        const def = this.config('ui.default.tags');
        if (def) { // Split
            def.split(',').forEach((tag) => {
                if (!added[tag]) {
                    result.push({
                        name: tag
                    });
                    added[tag] = true;
                }
            });
        };
        return result.sort((a, b) => {
            return b.count - a.count;
        });
    }

    async setContext(context) {
        const code = await this.cmd('context', context);
        if (code == 0) {
            this.notifyChange();
            return true;
        }
        return false;
    }

    async contexts() {
        const conf = this.config('context', true);
        let result = [];
        let current = 'none';
        for (let key in conf) {
            if (key == '') {
                current = conf.context;
            } else {
                // Save config
                result.push({
                    name: key.substr(1),
                    context: key.substr(1),
                    filter: conf[key],
                });
            }
        }
        if (!result.length) {
            return undefined;
        }
        result.splice(0, 0, {
            name: '(none)',
            context: 'none',
            filter: 'Context not set',
        });
        result.forEach((item) => {
            item.selected = current == item.context;
        });
        return result;
    }

    async reports(expanded) {
        const reg = /^(\S+)\s(.+)$/;
        let result = []; // List
        await this.call(['reports'], {
            eat: (line) => {
                const m = line.match(reg);
                if (m) {
                    const report = m[1].trim();
                    result.push({
                        name: report,
                        title: m[2].trim(),
                        special: specialReports.indexOf(report) != -1,
                    });
                }
            }
        }, this.streamNotify());
        if (!expanded && this.reportsSublist) { // Need only subset
            let map = {};
            result.forEach((item) => {
                map[item.name] = item;
            });
            return this.reportsSublist.map(report => map[report]).filter(report => report);
        };
        return result.filter((item, idx) => {
            return idx < result.length-1;
        });
    }

    async projects(expanded) {
        const reg = /^(\s*)(.+)\s(\d+)$/;
        let result = []; // List
        if (expanded) {
            let added = {};
            await this.call(['_unique', 'project'], {
                eat(line) {
                    const parts = line.split('.');
                    let full = '';
                    parts.forEach((p, idx) => {
                        full += `.${p}`;
                        if (added[full]) return;
                        result.push({
                            name: p,
                            indent: idx*2,
                            children: [],
                        });
                        added[full] = true;
                    });
                }
            }, this.streamNotify());
        } else {
            await this.call(['projects'], {
                eat(line) {
                    const m = line.match(reg);
                    if (m) {
                        result.push({
                            name: m[2].trim(),
                            indent: m[1].length,
                            count: parseInt(m[3], 10),
                            children: [],
                        });
                    }
                }
            }, this.streamNotify());
        }
        const processOne = (from, arr, indent, prefix) => {
            for (var i = from; i < result.length; i++) {
                let item = result[i];
                if (item.indent == indent) { // Same level
                    item.project = prefix+item.name;
                    if (item.name == '(none)') {
                        item.project='';
                    }
                    arr.push(item);
                }
                if (item.indent > indent) {
                    // children
                    let p = arr[arr.length-1];
                    i = processOne(i, p.children, item.indent, p.project+'.');
                    continue;
                }
                if (item.indent < indent) {
                    // Finish
                    return i-1;
                }
            }
            return result.length;
        };
        let roots = [];
        processOne(0, roots, 0, '');
        return roots;
    }

    async editConfig() {
        if (!this.provider.editConfig) { // Not available
            this.err('Not available on this platform');
            return;
        };
        try {
            await this.provider.editConfig();
        } catch (e) {
            console.log('Error:', e);
            if (e.code == 'no_editor') { // Show message
                return this.err('No editor app associated with plain-text files. Install one');
            };
            this.err('Edit error');
        }
    }

    async makeBackup() {
        try {
            await this.provider.backup();
        } catch (e) {
            console.log('Backup error:', e);
            this.err(e.message);
        }
    }

    calendar (from) {
        let dt;
        if (!from) { // First day
            dt = new Date();
        } else {
            dt = new Date(from.getTime());
        }
        dt.setDate(1);
        const m = dt.getMonth();
        const start = this.calendarConfig.start;
        const weekends = this.calendarConfig.weekends;
        if (dt.getDay() < start) {
            dt.setDate(1 + start  - dt.getDay() -7);
        } else {
            dt.setDate(1 + start - dt.getDay());
        }
        let result = []; // weeks
        do {
            let week = []; // days
            for (let i = 0; i < 7; i++) {
                week.push({
                    day: dt.getDate(),
                    date: isoDate(dt),
                    active: dt.getMonth() === m,
                    weekend: weekends.includes(dt.getDay()),
                });
                dt.setDate(dt.getDate()+1); // Next date
            };
            result.push(week);
        } while (dt.getMonth() == m);
        return result;
    }

    popupEnabled() {
        return this.panesConfig.popup.enabled || false;
    }

    async makePopupData() {
        const active = await this.count(this.panesConfig.popup.report, this.panesConfig.popup.filter);
        return {
            active: active > 0,
        }
    }
}
